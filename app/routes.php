<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::post('/like', array('uses' => 'DirectoryController@listingLike', 'as' => 'like'));
Route::post('/comment', array('uses' => 'DirectoryController@listingComment', 'as' => 'comment'));
Route::get('/comment', array('uses' => 'DirectoryController@index', 'as' => 'comment'));
Route::post('/get', array('uses' => 'DirectoryController@index', 'as' => 'filter'));
Route::post('/filter', array('uses' => 'DirectoryController@filter', 'as' => 'filter'));
Route::post('/searchpincode', array('uses' => 'DirectoryController@searchPincode', 'as' => 'searchpincode'));
Route::post('/uploadpic', array('uses' => 'DashboardController@uploadPic', 'as' => 'uploadpic'));
Route::post('/updaterow', array('uses' => 'DashboardController@updateRow', 'as' => 'updaterow'));
Route::post('/deleterow', array('uses' => 'DashboardController@destroy', 'as' => 'deleterow'));
Route::post('/searchlisting', array('uses' => 'DashboardController@searchListing', 'as' => 'searchlisting'));
Route::post('/updatelisting', array('uses' => 'DashboardController@update', 'as' => 'updatelisting'));


Route::get('/contact', function()
{
	return View::make('contact');
});
Route::get('/help', function()
{
	return View::make('help');
});

Route::get('/', array('uses' => 'HomeController@index'));
Route::get('/directory', array('uses' => 'DirectoryController@index'));
Route::post('/directory', array('uses' => 'DirectoryController@index'));
// OR Route::match(array('GET', 'POST'), '/directory', 'DirectoryController@index');


Route::get('/login', array('uses' => 'UsersController@showLogin'));
Route::post('/login', array('uses' => 'UsersController@doLogin'));

Route::get('/register', array('uses' => 'UsersController@showRegister'));
Route::post('/register', array('uses' => 'UsersController@doRegister'));

Route::get('/logout', array('uses' => 'UsersController@doLogout'));

Route::get('/dashboard', array('uses' => 'DashboardController@index'));

Route::post('/addlisting', array('uses' => 'DashboardController@store'));

Route::get('/view', array('uses' => 'DashboardController@updateview'));

