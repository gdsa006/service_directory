@if($searchResults->count() > 0)
@foreach($searchResults as $searchResult)
<li id="{{$searchResult -> pincode}}" onclick="$('.pincode').val(this.id);$('.searchResults').hide();$(this).preventDefault();">{{$searchResult -> pincode}} {{$searchResult -> city}}, {{$searchResult -> state}}</li>
@endforeach
@else
<li>Pincode not found</li>
@endif
