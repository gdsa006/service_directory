@extends('layout')
@include('navbar')
@section('content')

<section class="py-4" id="directory">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 sidebar collapse width show" id="sidebar">
                <div class="position-fixed h-100">
                @include('sidebar')
                </div>
            </div>
            <div class="col-md-7">
                <div class="col-md-12">
                    <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                        @if($listings->count() > 0 )
                        @else
                        No Results Found!
                        @endif
                        <div class="ajax_scroll" id="post-data">
                            @include('data')
                        </div>
                    </div>
                    <div class="ajax-load text-center" style="display:none">
                    <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More listings</p>
                </div>
                </div>
                
            </div>
            <div class="col-md-2 advertisements show">
                <div class="position-fixed1 h-100 w-100">
                    <div class="ad">
                        <img src="images/sample_ad.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('page-js-script')
<script>
var page = 1;
$(window).scroll(function () {
    if ($(window).scrollTop() + $(window).height() >= $(document).height()) {
        page++;
        loadMoreData(page);
    }
});

function loadMoreData(page) {
    $.ajax(
        {
            url: '?page=' + page,
            type: "get",
            beforeSend: function () {
                $('.ajax-load').show();
            },
            success: function (response) {
                console.log(response);
            }
        })
        .done(function (data) {
            if (data.html == "") {
                $('.ajax-load').html("<span class='endofResults'>That's it. Folks!</span>");
                return;
            }
            $('.ajax-load').hide();
            $("#post-data").append(data.html);
        })
        .fail(function (jqXHR, ajaxOptions, thrownError) {
            alert('server not responding...');
        });

}
</script>
@stop