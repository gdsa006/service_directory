@if (Auth::check())
@extends('layout')
@include('navbar')
@section('content')
@if(Session::has('alert-success'))
    <div class="alert alert-success">
        {{ Session::get('alert-success') }}
        <a class="btn btn-outline-success" href="view/#{{ Session::get('saved_id') }}"><strong>View</strong></a>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
@if(Session::has('alert-danger'))
    <div class="alert alert-danger">
        {{ Session::get('alert-danger') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<!-- <div class="alert alert-success">
  <strong>Success!</strong> Record Saved.
</div> -->
<section id="dashboard">
<div class="container">

    <div class="row">
    
<div class="col-6 text-center">
{{ Form::open(array('url' => 'uploadpic', 'class' => 'uploadPicForm', 'files' => 'true', 'enctype' => 'multipart/form-data', 'name' => 'uploadPicForm' )) }}

<div class="alert" id="message" style="display: none"></div>
    <div class="pic">
    </div>
    <input type="file" name="file" class="form-control uploadPic">
    <input type="submit" class="btn btn-primary btn-block" name="upload_pic" value="Upload">
    <span class="text-muted">Allowed files: jpg, png, gif only.</span>
    <div class="text-danger">{{ $errors->first('pic_filename') }}</div>
    {{ Form::close() }}
</div>


<div class="col-6">
{{ Form::open(array('url' => 'addlisting', 'class' => '', 'files' => 'true', 'enctype' => 'multipart/form-data' )) }}
    <input id="file_name" type="hidden" value="" name="pic_filename">
        <div class="form-group">
            <label for="exampleInputEmail1">Fullname</label>
            {{ Form::text('fullname', Input::old('fullname'), array('placeholder' => 'Enter fullname', 'class' => 'form-control', 'id' => 'inputPincode', 'value' => 'old("fullname")')) }}
            <div class="text-danger">{{ $errors->first('fullname') }}</div>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Phone</label>
            {{ Form::text('phone', Input::old('text'), array('placeholder' => 'Enter the phone', 'class' => 'form-control', 'id' => 'inputPincode')) }}        </div>
            <div class="text-danger">{{ $errors->first('phone') }}</div>
        <div class="form-group">
            <label for="exampleInputEmail1">Address</label>
            {{ Form::textarea('address', Input::old('text'), array('placeholder' => 'Enter the address', 'class' => 'form-control', 'id' => 'inputPincode')) }}
            <div class="text-danger">{{ $errors->first('address') }}</div>
        </div>
        <div class="form-group search">
            <label for="exampleInputEmail1">Pincode</label>
            {{ Form::text('pincode', Input::old('text'), array('placeholder' => 'Enter the pincode', 'class' => 'form-control pincode', 'id' => 'inputPincode')) }}
            <div class="text-danger">{{ $errors->first('pincode') }}</div>
            <ul class="searchResults">
            </ul>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Gender</label>
            <select class="form-control" name="gender">
            <option value="select"> Select </option>
            <option value="1"> Male </option>
            <option value="0"> Female </option>
            </select>
            <div class="text-danger">{{ $errors->first('gender') }}</div>
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1">Category</label>
            <select class="form-control" name="category">
                <option value="select"> Select  </option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->categ_name }}</option>
                @endforeach
            </select>
            <div class="text-danger">{{ $errors->first('category') }}</div>

        </div>
        <div class="form-group">
            <input type="submit" value="Submit" class="btn btn-primary btn-block">
        </div>
        {{ Form::close() }}
    </div>
</div>
</div>
</section>

@stop
@endif