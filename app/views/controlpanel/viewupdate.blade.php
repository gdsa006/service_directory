@if (Auth::check())
@extends('layout')
@include('navbar')
@section('content')
<section id="dashboard">
<div class="container">

    <div class="row">


    {{ Form::open(array('url' => 'searchlisting', 'class' => 'w-100' )) }}
  <div class="form-group search">
  {{ Form::text('listingsearch', Input::old('text'), array('placeholder' => 'Search by Name/Address/Phone/Pincode', 'class' => 'form-control listing_search w-100', 'id' => 'searchListing', 'autofocus')) }}
  </div>
  {{ Form::close() }}
  
        <table class="table table-striped">
    <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Fullname</th>
      <th scope="col">Phone</th>
      <th scope="col">Address</th>
      <th scope="col">Pincode</th>
      <th scope="col">Gender</th>
      <th scope="col">Category</th>
      <th scope="col">Pic</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody class="display_results">
  
  </tbody>
</table>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="listingPhoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="../../uploads/pic.png" class="img-fluid" id="modal_listingPhoto">
      </div>
     
    </div>
  </div>
</div>


@stop
@endif



