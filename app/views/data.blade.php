@foreach($listings as $list)
<div class="panel panel-default item">
    <div class="panel-heading" role="tab" id="heading-{{ $list->id }}">
        <span class="toggle float-right collapsed " role="button" data-toggle="collapse" data-parent="#accordion" href="#listing-{{ $list->id }}" aria-expanded="true" aria-controls="listing-0">
        </span>
        <span class="user-interact">
            <a href="#" class="like {{ Like::where('listing_id', $list->id)->where('ip', '616556550')->first() ? Like::where('listing_id', $list->id)->where('ip', '616556550')->first()->like == 1 ? 'active-like' : '' : '' }}" id="{{ $list->id }}" data-likes="{{ Like::where('listing_id', $list->id)->where('like', '1')->count();}}"></a>
            <a href="#" class="like {{ Like::where('listing_id', $list->id)->where('ip', '616556550')->first() ? Like::where('listing_id', $list->id)->where('ip', '616556550')->first()->like == 0 ? 'active-dislike' : '' : '' }}" id="{{ $list->id }}" id="{{ $list->id }}" data-dislikes="{{ Like::where('listing_id', $list->id)->where('like', '0')->count();}}"></a>
        </span>
        <span class="pic">
            @if($list->photo)
            <img src="images/uploads/{{$list->photo}}">
            @else
            <img src="images/pic.png">
            @endif
        </span>
        <h4 class="panel-title name float-left">
            {{ $list->fullname }}
            <span class="rating">
                <span class="rating-hider" data-likes="{{$likes = Like::where('listing_id', $list->id)->where('like', '1')->count();}}" data-dislikes="{{$dislikes = Like::where('listing_id', $list->id)->where('like', '0')->count();}}" style="width: @if($likes == $dislikes) 50% @else @if($likes > $dislikes) calc(95% - (({{$likes}})/({{$likes}} + {{$dislikes}}))*95%);  @else calc((({{$dislikes}})/({{$likes}} + {{$dislikes}}))*95%); @endif @endif">

                </span>
                <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
            </span>
        </h4>
        <span class="rating">
            <span class="rating-hider" data-likes="{{$likes = Like::where('listing_id', $list->id)->where('like', '1')->count();}}" data-dislikes="{{$dislikes = Like::where('listing_id', $list->id)->where('like', '0')->count();}}" style="width: @if($likes == $dislikes) 50% @else @if($likes > $dislikes) calc(95% - (({{$likes}})/({{$likes}} + {{$dislikes}}))*95%);  @else calc((({{$dislikes}})/({{$likes}} + {{$dislikes}}))*95%); @endif @endif">

            </span>
            <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
        </span>
    </div>
    <div id="listing-{{ $list->id }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne-{{ $list->id }}">
        <div class="panel-body">
            <div class="row row-eq-height">
                <div class="col-md-7">
                    <div class="comments">
                        <h5 class="mb-3">Comments(<span>{{ $list->comments->count() }}</span>)</h5>
                        <div class="inner-scroll inner-scroll-{{ $list->id }}" id="myBox">
                            @include('data-comments')
                        </div>
                        <a class="btn btn-basic btn-sm" id="comment-btn">Comment</a>
                        <div class="comment-box text-center">
                            <textarea placeholder="enter your comment here..." class="form-control" name="comment"></textarea>
                            <a href="/comment" class="submit-comment btn btn-main btn-block" data-listingid="{{ $list->id }}">Submit</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="contact">
                        <h5 class="mb-3">Contact</h5>
                        <div class="info phone mb-2">
                            <i class="fa fa-phone"></i> {{ $list->phone }}
                        </div>
                        <div class="info address mb-2">
                            <i class="fa fa-home"></i> {{ $list->address }}, {{ $list->pincode     }}
                        </div>
                        <div class="info gender mb-2">
                            <i class="fa fa-user"></i> {{ $list->gender == '1' ? 'Male' : 'Female' }}
                        </div>
                        <div class="info gender mb-2">
                            <i class="fa fa-th"></i> {{ $list->categories_id }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach                        