<div class="filters">
{{ Form::open(array('url' => 'directory')) }}

                    <div class="form-group">
                        
                        <h5 class="mb-3">Gender</h5>
                        <div class="form-check">
                            <label class="form-check-label">
                            <div class="form-group">
                            <input type="checkbox" id="male" name="gender_male" class="gender d-none" @if($gender_male == '1') value="0" @else value="1" @endif data-toggle="toggle" data-width="100" data-on="Male Only" data-off="All" @if($gender_male == '1') checked @endif>
                            <a for="male" @if($gender_male == '1') class="gender btn btn-primary btn-sm active" @else class="gender btn btn-primary btn-sm" @endif ><i class="fa fa-male" aria-hidden="true"></i> Male Only</a>
                            </div>
                            </label>
                            <label class="form-check-label">
                            <div class="form-group">
                            <input type="checkbox" id="female" name="gender_female" class="gender d-none" @if($gender_female == '1') value="0" @else value="1" @endif data-toggle="toggle" data-width="100" data-on="Male Only" data-off="All" @if($gender_female == '1') checked @endif>
                            <a for="female" @if($gender_female == '1') class="gender btn btn-primary btn-sm active" @else class="gender btn btn-primary btn-sm" @endif data-toggle="toggle" data-width="100" data-on="Male Only" data-off="All" @if($gender_female == '1') checked @endif><i class="fa fa-female" aria-hidden="true"></i> Female Only</a>
                            </div>
                            </label>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <h5 class="mb-3">Rating</h5>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input d-none" id="rating_5" onchange="submit();" name="rating_5" @if($rating_5 == '1') value="0" @else value="1" @endif  @if($rating_5 == '1') checked @endif>
                                <a for="rating_5" @if($rating_5 == '1') class="rating active" @else class="rating" @endif>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </a>
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input d-none" onchange="submit();" name="rating_4" @if($rating_4 == '1') value="0" @else value="1" @endif  @if($rating_4 == '1') checked @endif>
                                <a for="rating_4" @if($rating_4 == '1') class="rating active" @else class="rating" @endif>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </a>
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input d-none" onchange="submit();" name="rating_3" @if($rating_3 == '1') value="0" @else value="1" @endif  @if($rating_3 == '1') checked @endif>
                                <a for="rating_3" @if($rating_3 == '1') class="rating active" @else class="rating" @endif>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </a>
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input d-none" onchange="submit();" name="rating_2" @if($rating_2 == '1') value="0" @else value="1" @endif  @if($rating_2 == '1') checked @endif>
                                <a for="rating_2" @if($rating_2 == '1') class="rating active" @else class="rating" @endif>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </a>
                            </label>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input d-none" onchange="submit();" name="rating_1" @if($rating_1 == '1') value="0" @else value="1" @endif  @if($rating_1 == '1') checked @endif>
                                <a for="rating_1" @if($rating_1 == '1') class="rating active" @else class="rating" @endif>
                                    <i class="fa fa-star"></i>
                                    &nbsp;
                                </a>
                            </label>
                        </div>
                    </div>
                    @if($gender_male == '1' || $gender_female == '1' || $rating_1 == '1' || $rating_2 == '1' || $rating_3 == '1' || $rating_4 == '1' || $rating_5 == '1')
                            <div class="form-group mt-4">
                            <h5 class="mb-3">&nbsp;</h5>
                            <a id="clear" name="gender_clear" class="btn btn-warning btn-sm btn-block"  >clear filters</a>
                            </div>
                            @endif
                    {{ Form::close() }}

                </div>
                
<div class="rankings mt-3">
    <h5 class="mb-3">World Rankings</h5>
    <table class="table table-bordered table-hover">
        <tbody>
            @foreach($rankings as $rank)
            <tr>
                <td>{{ $rank->fullname }}</td>
                <td>{{ $rank->address }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>