<nav class="navbar navbar-expand-lg fixed-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">Doodo!!</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample07">
        @if (Auth::check())
        <ul class="navbar-nav" style="position: absolute; left: 50%; transform: translateX(-50%);">
        <li class="nav-item">
                        <a class="nav-link">Welcome Admin!</a>
                    </li>
        </ul>
        <ul class="navbar-nav ml-auto">
       
                    <li class="nav-item">
                        <a class="nav-link" href="/dashboard">Dashboard/Add</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/view">Update/View</a>
                    </li>
                <li class="nav-item">
                        <a class="nav-link" href="/logout">Logout</a>
                    </li>
            </ul>
            @else
            <ul class="navbar-nav mr-auto">
                <li class="nav-item {{ (Request::is('*home') ? 'active' : '') }}">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item {{ (Request::is('*directory') ? 'active' : '') }}">
                    <a class="nav-link" href="{{ URL::to('/directory') }}">Directory</a>
                </li>
                <li class="nav-item {{ (Request::is('*contact') ? 'active' : '') }}">
                    <a class="nav-link" href="{{ URL::to('/contact') }}">Contact Us</a>
                </li>
                <li class="nav-item {{ (Request::is('*help') ? 'active' : '') }}">
                    <a class="nav-link" href="/help">Help</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    @if(Session::has('pincode'))
                    <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-map-marker-alt"></i> {{ Session::get('pincode')}}
                    </a>
                    @else
                    <a class="btn btn-warning" data-toggle="modal" data-target="#exampleModal">
                        Set Pincode
                    </a>
                    @endif
                </li>
            </ul>
            @endif
        </div>
    </div>
</nav>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Enter Pincode</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ Form::open(array('url' => 'directory')) }}
                <div class="input-group">
                    {{ Form::text('pincode', Input::old('text'), array('placeholder' => 'Enter the pincode', 'class' => 'form-control', 'id' => 'inputPincode')) }}
                    <div class="input-group-append">
                        <input type="submit" class="btn btn-primary btn-main" value="Find" id="external_pincode">
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>