@extends('layout')
@section('content')
@if(Session::has('alert-danger'))
    <div class="alert alert-danger">
        {{ Session::get('alert-danger') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
@endif
<div class="container h-100" id="login">
    <div class="row align-items-center h-100">
        <div class="col-5 mx-auto">
        <div class="card">
            <div class="card-header text-center">
                Welcome to Doodo!! Admin.
            </div>
                    <div class="card-body">
                        {{ Form::open(array('url' => 'login')) }}
                        <div class="form-label-group mb-3">
                            <label for="inputEmail">Email</label>
                            {{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com', 'class' => 'form-control', 'id' => 'inputEmail' )) }}
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                        <div class="form-label-group">
                            <label for="inputPassword">Password</label>
                            {{ Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword')) }}
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                        <div class="form-label-group mt-5">
                        {{ Form::submit('Sign in', array('class' => 'btn btn-lg btn-primary btn-block text-uppercase btn-blue')) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
        </div>
    </div>
</div>


<style>
    body{
    background-image: url(../images/5964.png);
    background-attachment: fixed;
    padding: 0
}
</style>
@stop