@extends('layout')

@section('content')
<section>
    <div class="container">
        <div class="row my-flex-card">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-body">
                        {{ Form::open(array('url' => 'register')) }}
                        <div class="form-label-group mb-3">
                            <label for="inputEmail">Email</label>
                            {{ Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com', 'class' => 'form-control', 'id' => 'inputEmail' )) }}
                        </div>
                        <div class="form-label-group mb-3">
                            <label for="inputFullname">Fullname</label>
                            {{ Form::text('fullname', Input::old('text'), array('placeholder' => 'Name', 'class' => 'form-control', 'id' => 'inputFullname' )) }}
                        </div>
                        <div class="form-label-group mb-3">
                            <label for="inputPassword">Password</label>
                            {{ Form::password('password', array('class' => 'form-control', 'id' => 'inputPassword')) }}
                        </div>
                        <div class="form-label-group">
                            <label for="inputConfirmPassword">Confirm Password</label>
                            {{ Form::password('confirmpassword', array('class' => 'form-control', 'id' => 'inputConfirmPassword')) }}
                        </div>
                        <div class="form-label-group mt-5">
                        {{ Form::submit('Sign up', array('class' => 'btn btn-lg btn-primary btn-block text-uppercase btn-blue')) }}
                            <a class="d-block text-center mt-2 small" href="login.html">Already registered? Login here.</a>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
</section>
@stop