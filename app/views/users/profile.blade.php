@extends('layout')

@section('content')
<section>
        <div class="container">
            <div class="row my-flex-card">
                <div class="col-md-6">
                    <div class="card post post-2">
                        <div class="card-header text-center">
                            <span class="font-weight-bold">Profile Information</span>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                
                                    <tr>
                                        <td class="border-top-0">Name</td>
                                        <td class="border-top-0">{{ $users->fullname }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{ $users->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>DOB</td>
                                        <td>{{ $users->dob }}</td>
                                    </tr>
                                    <tr>
                                        <td>Gender</td>
                                        <td>{{ $users->gender }}</td>
                                    </tr>
                                    <tr>
                                        <td>Location</td>
                                        <td>{{ $users->location }}</td>
                                    </tr>
                                </tbody>
                            </table>    
                        </div>
                        <div class="card-footer">
                            <div class="info-text text-center">
                                <span><a href="#" class="btn btn-primary btn-blue d-none">Update Profile</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card post post-2">
                        <div class="card-header text-center">
                            <span class="font-weight-bold">Wall Status</span>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td class="border-top-0">DreamPosts</td>
                                        <td class="border-top-0">{{ $posts_count }}</td>
                                    </tr>
                                    <tr>
                                        <td>Loves</td>
                                        <td>{{ $likes }}</td>
                                    </tr>
                                    <tr>
                                        <td>Hated</td>
                                        <td>{{ $dislikes }}</td>
                                    </tr>
                                    <tr>
                                        <td>Last Post</td>
                                        <td>{{ $last_post->created_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="info-text text-center">
                                <span><a href="#" class="btn btn-primary btn-blue d-none">View Posts</a></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 d-none">
                    <div class="card post post-2">
                        <div class="card-header text-center">
                            <span class="font-weight-bold">Connections</span>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td class="border-top-0">Starred</td>
                                        <td class="border-top-0">1323</td>
                                    </tr>
                                    <tr>
                                        <td>Viewed</td>
                                        <td>145242</td>
                                    </tr>
                                    <tr>
                                        <td>Last Connect</td>
                                        <td>Giandeep</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <div class="info-text text-center">
                                <span><a href="#" class="btn btn-primary btn-blue">View All</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
@stop