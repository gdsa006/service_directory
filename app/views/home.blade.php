@extends('layout')

@section('content')
<section id="home">
    <img src="images/5964.png">
    <div class="caption">
        <h1>Doodo!!</h1>
        {{ Form::open(array('url' => 'directory', 'class' => 'form-inline' )) }}
        <div class="form-group search">
        {{ Form::text('pincode', Input::old('text'), array('placeholder' => 'Enter the pincode', 'class' => 'form-control pincode', 'id' => 'inputPincode')) }}
            <ul class="searchResults">
            </ul>
  </div>
  <div class="form-group">

  <select class="form-control data_source" name="categories[]" multiple="multiple">
                <option value=""> Select Category </option>
                @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->categ_name }}</option>
                @endforeach
            </select>
  </div>
  <input type="submit" class="btn btn-primary btn-main" value="Find">
        {{ Form::close() }}
    </div>
    <div class="home-footer">
        <ul class="list-inline">
            <li class="list-inline-item">
                <a href="/directory">
                    Directory
                </a>
            </li>
            <li class="list-inline-item">
                <a href="/contact">
                    Contact Us
                </a>
            </li>
            <li class="list-inline-item">
                <a href="/help">
                    Help
                </a>
            </li>
        </ul>
    </div>
</section>
<style>
    body {
        padding: 0;
        overflow: auto
    }
</style>
@stop