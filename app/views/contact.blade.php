@extends('layout')
@include('navbar')
@section('content')
<section id="contactus">
    <div class="container">
        <div class="row row-eq-height">
            <div class="col-md-6">
                <div class="left h-100">
                    <div class="inner">
                        <h3>Drop an email to</h3>
                        <a href="mailto: gdsa006@gmail.com">
                            gdsa006@gmail.com
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="right">
                    <div class="inner">
                        
                        <form class="col-sm-10 offset-1">
                        <h3>Quick Contact</h3>
                            <div class="contact-form">
                                <div class="form-group">
                                    <label class="control-label col" for="fname">First Name:</label>
                                    <div class="col">
                                        <input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col" for="lname">Last Name:</label>
                                    <div class="col">
                                        <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col" for="email">Email:</label>
                                    <div class="col">
                                        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col" for="comment">Comment:</label>
                                    <div class="col">
                                        <textarea class="form-control" rows="5" id="comment"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-2 col-md-10">
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop