
@if($searchResults->count() > 0)

@foreach($searchResults as $searchResult)

<tr id="update-{{ $searchResult->id }}" style="display: none">
{{ Form::open(array('url' => 'updatelisting', 'class' => 'updateListingForm-' . $searchResult->id, 'files' => 'true', 'enctype' => 'multipart/form-data', 'name' => 'updateListingForm-' . $searchResult->id )) }}
    <td>{{ $searchResult->id }}</td>
    <td><input type="text" id="fullname-{{ $searchResult->id }}" class="w-100" value="{{ $searchResult->fullname }}" name="fullname"></td>
    <td><input type="text" id="phone-{{ $searchResult->id }}" class="w-100" value="{{ $searchResult->phone }}" name="phone"></td>
    <td><input type="text" id="address-{{ $searchResult->id }}" class="w-100" value="{{ $searchResult->address }}" name="address"></td>
    <td><input type="text" id="pincode-{{ $searchResult->id }}" class="w-100" value="{{ $searchResult->pincode }}" name="pincode"></td>
    <td><input type="text" id="gender-{{ $searchResult->id }}" class="w-100" value="{{ $searchResult->gender }}" name="gender"></td>
    <td><input type="text" id="category-{{ $searchResult->id }}" class="w-100" value="{{ $searchResult->categories_id }}" name="category"></td>
    <td colspan="2"><a data-id="{{ $searchResult->id }}" class="update text-warning">Update</a></td>
    <td colspan="1"><button class="edit text-danger" value="cancel" data-id="update-{{ $searchResult->id }}">Cancel</button></td>
{{ Form::close() }}
</tr>

<tr id="{{ $searchResult->id }}">
    <td>{{ $searchResult->id }}</td>
    <td>{{ $searchResult->fullname }}</td>
    <td>{{ $searchResult->phone }}</td>
    <td>{{ $searchResult->address }}</td>
    <td>{{ $searchResult->pincode }}</td>
    <td>@if(($searchResult->gender)) Male @else Female @endif</td>
    <td>{{ $searchResult->categories_id }}</td>
    <td><a class="open-photoListing" data-photo="{{$searchResult->photo}}" data-toggle="modal" data-target="#listingPhoto">View</a></td>
    <td><a id="edit-{{ $searchResult->id }}" class="edit text-success">Edit</a></td>
    <td><a class="delete text-danger">Delete</a></td>
</tr>

@endforeach

@else
No results found!
  @endif