<?php
use Illuminate\Http\Request;
class Listing extends Eloquent{

	protected $fillable = ['fullname', 'phone', 'address', 'pincode', 'gender'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'listings';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('');

    public function likes()
	{
		return $this->hasMany('Like');
    }
    
    public function comments()
	{
		return $this->hasMany('Comment');
	}
}   
