<?php
use Illuminate\Http\Request;
class Category extends Eloquent
{


	protected $fillable = ['id', 'categ_name'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('remember_token');
    
    
    //for like dislike
    
    public function listing()
	{
		return $this->belongsTo('Listing');
	}
	
	//public function getAllPosts() {
	//	return Post::with('User')->get();
	//}

}
