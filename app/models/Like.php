<?php

class Like extends Eloquent
{


	protected $fillable = ['listing_id', 'ip', 'like'];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'likes';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('remember_token');
    
    
    //for like dislike
    
    public function listing()
	{
		return $this->belongsTo('Listing');
	}
	
	//public function getAllPosts() {
	//	return Post::with('User')->get();
	//}

}
