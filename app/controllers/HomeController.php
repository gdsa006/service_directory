<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		$category = new Category();
		$categories = $category->all();
		Session::flush();
		setcookie('pincode_cookie', '0', time() + (86400 * 30), "/");
		setcookie('categories_cookie', '0', time() + (86400 * 30), "/");
		setcookie('gender_male_cookie', '0', time() + (86400 * 30), "/");
		return View::make('home', compact('categories'));
	}

	public function search()
	{
		echo 'aaa';
	}

}
