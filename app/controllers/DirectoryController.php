<?php

use Illuminate\Support\Facades\Response as FacadeResponse;

class DirectoryController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Input::has('pincode')){
			$pincode = Input::get('pincode');
			Session::put('pincode', $pincode);
		}

		if (Input::has('categories')){
			$categories = array();
			$categories = Input::get('categories');
			Session::put('categories', $categories);
		}

		$pincode = Session::get('pincode');
		$categories = Session::get('categories');

		$gender_male = Input::get('gender_male') == '1' ? '1':'0';
		$gender_female = Input::get('gender_female') == '1' ? '1':'0';

		$rating_1 = Input::get('rating_1') == '1' ? '1':'0';
		$rating_2 = Input::get('rating_2') == '1' ? '1':'0';
		$rating_3 = Input::get('rating_3') == '1' ? '1':'0';
		$rating_4 = Input::get('rating_4') == '1' ? '1':'0';
		$rating_5 = Input::get('rating_5') == '1' ? '1':'0';
		$rating_highest = Listing::max('likes_count');

		if($gender_male == '1' || $gender_female == '1'){
			setcookie('gender_male_cookie', $gender_male, time() + (86400 * 30), "/");
			setcookie('gender_female_cookie', $gender_female, time() + (86400 * 30), "/");
		}

		if($rating_1 == '1' || $rating_2 == '1' || $rating_3 == '1' || $rating_4 == '1' || $rating_5 || '1'){
			setcookie('rating_1_cookie', $rating_1, time() + (86400 * 30), "/");
			setcookie('rating_2_cookie', $rating_2, time() + (86400 * 30), "/");
			setcookie('rating_3_cookie', $rating_3, time() + (86400 * 30), "/");
			setcookie('rating_4_cookie', $rating_4, time() + (86400 * 30), "/");
			setcookie('rating_5_cookie', $rating_5, time() + (86400 * 30), "/");
		}

		if ($pincode || $categories) {
			setcookie('pincode_cookie', $pincode, time() + (86400 * 30), "/");
			setcookie('categories_cookie', json_encode($categories), time() + (86400 * 30), "/");
		}

		if ($_COOKIE['gender_male_cookie'] == '1') {
			$gender_male = $_COOKIE['gender_male_cookie'];
		}

		$listings = Listing::with(array('comments' => function ($query) {
			$query->orderBy('created_at', 'DSC');
		}))->where(function ($q) use ($pincode, $categories, $gender_male, $gender_female, $rating_1, $rating_2, $rating_3, $rating_4, $rating_5) {
		if($pincode)
			$q->where('pincode', $pincode);
		if($categories)
			$q->whereIn('categories_id', $categories);
		if($gender_male){
			$q->where('gender', '1');
		}
		if($gender_female){
			$q->where('gender', '0');
		}
		if($rating_1){
			$q->where('likes_count', '<' , '7');
		}
		if($rating_2){
			$q->where('likes_count', '>=' , '8');
			$q->where('likes_count', '<=' , '11');
		}
		if($rating_3){
			$q->where('likes_count', '>=' , '12');
			$q->where('likes_count', '<=' , '15');
		}
		if($rating_4){
			$q->where('likes_count', '>=' ,'16');
			$q->where('likes_count', '<=' ,'20');
		}
		if($rating_5){
			$q->where('likes_count', '>' , '21');
		}
		})->paginate(4);

		if (Request::ajax()) {
			if (isset($_COOKIE['pincode_cookie'])) {
				$pincode = $_COOKIE['pincode_cookie'];
			}
			if (isset($_COOKIE['categories_cookie'])) {
				$categories = json_decode($_COOKIE['categories_cookie'], true);
			}
			if (isset($_COOKIE['gender_male_cookie'])) {
				$gender_male = $_COOKIE['gender_male_cookie'];
			}
			if (isset($_COOKIE['gender_female_cookie'])) {
				$gender_female = $_COOKIE['gender_female_cookie'];
			}
			if (isset($_COOKIE['$rating_1_cookie'])) {
				$rating_1 = $_COOKIE['$rating_1_cookie'];
			}
			if (isset($_COOKIE['$rating_2_cookie'])) {
				$rating_2 = $_COOKIE['$rating_2_cookie'];
			}
			if (isset($_COOKIE['$rating_3_cookie'])) {
				$rating_3 = $_COOKIE['$rating_3_cookie'];
			}
			if (isset($_COOKIE['$rating_4_cookie'])) {
				$rating_4 = $_COOKIE['$rating_4_cookie'];
			}
			if (isset($_COOKIE['$rating_5_cookie'])) {
				$rating_5 = $_COOKIE['$rating_5_cookie'];
			}

			$listings = Listing::with(array('comments' => function ($query) {
				$query->orderBy('created_at', 'DSC');
			}))->where(function ($q) use ($pincode, $categories, $gender_male, $gender_female, $rating_1, $rating_2, $rating_3, $rating_4, $rating_5) {
			if($pincode)
				$q->where('pincode', $pincode);
			if($categories)
				$q->whereIn('categories_id', $categories);
			if($gender_male){
				$q->where('gender', '1');
			}
			if($gender_female){
				$q->where('gender', '0');
			}
			if($rating_1){
				$q->where('likes_count', '<' , '7');
			}
			if($rating_2){
				$q->where('likes_count', '>=' , '8');
				$q->where('likes_count', '<=' , '11');
			}
			if($rating_3){
				$q->where('likes_count', '>=' , '12');
				$q->where('likes_count', '<=' , '15');
			}
			if($rating_4){
				$q->where('likes_count', '>=' ,'16');
				$q->where('likes_count', '<=' ,'20');
			}
			if($rating_5){
				$q->where('likes_count', '>' , '21');
			}
			})->paginate(4);

			$view = View::make('data', compact('listings', 'rankings', 'gender_male', 'gender_female'))->render();
            return Response::json(['html' => $view]);
		}
		
		$lists = new Listing();
		$rankings = Listing::select('fullname', 'address')->orderBy('likes_count', 'DESC')->limit(5)->get();
		return View::make('directory', compact('listings', 'rankings', 'gender_male', 'gender_female', 'rating_1', 'rating_2', 'rating_3', 'rating_4', 'rating_5'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function listingLike()
	{
		if (Request::ajax()) {
			$listing_Id = Input::get('listingId');
			$isLike = Input::get('isLike') === 'true';
			$update = false;
			$listing = Listing::find($listing_Id);
			if (!$listing) {
				return null;
			}
			$userip = '616556550';
			$like = Like::where('listing_Id', $listing_Id)->where('ip', $userip)->first();
			if ($like) {
				$already_like = $like->like;
				$update = true;
				if ($already_like == $isLike) {
					$like->delete();
					return null;
				}
			} else {
				$like = new Like();
			}
			$like->like = $isLike;
			$like->ip = $userip;
			$like->listing_id = $listing->id;
			if ($update) {
				if ($isLike == true) {
					$listing->likes_count = $listing->likes_count + 1;
					$listing->update();
					$like->update();
				} else {
					$listing->likes_count = $listing->likes_count - 1;
					$listing->update();
					$like->update();
				}
			} else {
				if ($isLike == true) {
					$listing->likes_count = $listing->likes_count + 1;
					$listing->save();
					$like->save();
				} else {
					$listing->likes_count = $listing->likes_count - 1;
					$like->save();
				}
			}
			return null;
		}
	}


	public function listingComment()
	{
		if (Request::ajax()) {
			$listing_Id = Input::get('listingId');
			$isComment = Input::get('isComment');
			$listing = Listing::find($listing_Id);
			if (!$listing) {
				return null;
			}
			$userip = '616556550';
			$comment = new Comment();
			$comment->listing_id = $listing_Id;
			$comment->ip = $userip;
			$comment->comment = $isComment;
			$comment->save();
			return $isComment;
		}
	}
	
	public function searchPincode(){
		if (Request::ajax()) {
			$searchTerm = Input::get('getSearchTerm');
			$searchResults = Pincode::where('pincode', 'like', '%' . $searchTerm . '%')->get();
			$view = View::make('search-results', compact('searchResults'))->render();
            return Response::json(['html' => $view]);
		}
	}
}

	