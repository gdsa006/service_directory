<?php

class UsersController extends \BaseController {

	public function showLogin() {
        return View::make('users/login');
    }

    public function doLogin()
    {
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        }
        else {

             // create our user data for the authentication
            $userdata = array(
                'email'     => Input::get('email'),
                'password'  => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {
                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
                echo 'LOGIN SUCCESS';
                return Redirect::to('dashboard');
            } 
            else 
            {        
                // validation not successful, send back to form 
                return Redirect::to('login')->with('alert-danger', 'User does not exists');
            }
        }
    }

    public function showRegister() {
        return View::make('users/register');
    }

    public function doRegister()
    {
        $rules = array(
            'email'    => 'required|email', // make sure the email is an actual email
            'fullname'    => 'required', // make sure the email is an actual email
            'password' => 'required',// password can only be alphanumeric and has to be greater than 3 characters
            'confirmpassword' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
        );

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) { //optional ($validator->passes())
            return Redirect::to('register')
                ->with('message', 'The following errors occurred')
                ->withErrors($validator); // send back all errors to the login form
        }
        else {
             // create our user data for the authentication
             $user = new User;
             $user->email    = Input::get('email');
             $user->fullname = Input::get('fullname');
             $user->password = Hash::make(Input::get('password'));
             $user->save();
             return Redirect::to('login');
        }
    }

    public function doLogout()
    {
        //Auth::logout(); // log the user out of our application // not working properly
        Session::flush();
        return Redirect::to('login'); // redirect the user to the login screen
    }

    public function showDetails() {
        $users = Auth::user();
        $post = new Post;
        $posts_count = $users->posts()->where('user_id',$users->id)->count();
        $last_post = $users->posts()->select('created_at')->latest()->first();
        $likes = $users->likes()->where('user_id',$users->id)->where('like', '=' ,'1')->count();
        $dislikes = $users->likes()->where('user_id',$users->id)->where('like', '=' ,'0')->count();
		return View::make('/users/profile', compact('users','posts_count','last_post','likes','dislikes'));    
}



}
