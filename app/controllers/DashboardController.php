<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class DashboardController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function updateview()
	{
		if (Auth::check()){
			$listings = Listing::paginate(12);
			return View::make('controlpanel/viewupdate', compact('listings'));
		}else{
			echo 'please <a href="/login">login</a>!';
		}
	}

	public function updateRow(){
		if (\Request::ajax()) {
			$rowID = Input::get('rowID');
			return $rowID;
		}	
	}


	public function index()
	{
		if (Auth::check()){
			$categories = Category::all();
			return View::make('controlpanel/dashboard', compact('categories'));
		}else{
			echo 'please <a href="/login">login</a>!';
		}
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$variable = "select";
		$rules = array(
			'fullname'    => 'required|min:5|max:13',
			'phone'    => 'required|numeric',
			'address' => 'required',
			'pincode' => 'required|numeric',
			'gender' => 'required|not_in:'.$variable,
			'pic_filename' => 'required',
			'category' => 'required|not_in:'.$variable
        );
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->fails()) { 
			return Redirect::back()->withInput()->with('alert-danger', 'The data wasn not saved successfully')->withErrors($validator);
		}
		else{
		$category = array();
		$fullname = Input::get('fullname');
		$phone = Input::get('phone');
		$address = Input::get('address');
		$pincode = Input::get('pincode');
		$gender = Input::get('gender');
		$category = Input::get('category');
		$pic = Input::get('pic_filename');
		$listing = new Listing();
		$listing->fullname = $fullname;
		$listing->phone = $phone;
		$listing->address = $address;
		$listing->pincode = $pincode;
		$listing->gender = $gender;
		$listing->photo = $pic;
		$listing->categories_id = $category;
		$listing->save();
		return Redirect::back()->with('alert-success', 'The data was saved successfully')->with('saved_id', $listing->id);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update()
	{
		if (\Request::ajax()) {
		$variable = "select";
		$rules = array(
			'fullname'    => 'required|min:5|max:13',
			'phone'    => 'required|numeric',
			'address' => '',
			'pincode' => 'required|numeric',
			'gender' => 'required|not_in:'.$variable,
			'category' => 'required|not_in:'.$variable
        );
			$validator = Validator::make(Input::all(), $rules);
			if ($validator->fails()) { 
				return Response::json([
					'message'   => 'Validation Error',
					'class_name'  => 'alert-warning',
				   ]);
			}
			else{
				$listing = new Listing();
				$listing = $listing->find(Input::get('id'));
				$photo = $listing->photo;
				$listing->fullname = Input::get('fullname');
				$listing->phone = Input::get('phone');
				$listing->address = Input::get('address');
				$listing->pincode = Input::get('pincode');
				$listing->gender = Input::get('gender');
				$listing->categories_id = Input::get('category');
				$listing->photo = $photo;
				$listing->update();
				if($listing->update()){
					return Response::json([
						'message'   => 'Updated',
						'fullname' => Input::get('fullname'),
						'phone' => Input::get('phone'),
						'address' => Input::get('address'),
						'pincode' => Input::get('pincode'),
						'gender' => Input::get('gender'),
						'category' => Input::get('category'),
						'photo' => $photo
					   ]);
				}
				else{
					return Response::json([
						'message'   => 'Some Error Occured.',
						'class_name'  => 'alert-danger',
						'file_name' => $variable
					   ]);
				}
				return 'Updated';
			}
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		if (\Request::ajax()) {
			$rowID = Input::get('rowID');
			$listing = new Listing();
			$listing = $listing->find($rowID);
			$listing->delete();
			return 'Deleted';
		}
	}
	
	public function uploadPic(){
		
		$validation = Validator::make(\Request::all(), [
			'file' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
		   ]);
		   if (\Request::ajax()) {
		   if($validation->passes())
		   {
			$image = \Request::file('file');
			$new_name = rand() . '.' . $image->getClientOriginalExtension();
      		$image->move(public_path('images/uploads'), $new_name);
      		return Response::json([
       		'message'   => 'Image Upload Successfully',
       		'uploaded_image' => '<img src="/images/uploads/'.$new_name.'" class="img-thumbnail" width="300" />',
			   'class_name'  => 'alert-success',
			   'file_name' => $new_name
      		]);
		   }
		   else
			{
			return Response::json([
			'message'   => $validation->errors()->all(),
			'uploaded_image' => '',
			'class_name'  => 'alert-danger'
			]);
			}
		
	}
	}

	public function searchListing(){
		if (\Request::ajax()) {
			$searchTerm = Input::get('getSearchTerm');
			$searchResults = Listing::where(function ($q) use ($searchTerm) {
				if($searchTerm)
					$q->where('fullname', 'like', '%' . $searchTerm . '%');
					$q->orWhere('address', 'like', '%' . $searchTerm . '%');
					$q->orWhere('pincode', 'like', '%' . $searchTerm . '%');
					$q->orWhere('phone', 'like', '%' . $searchTerm . '%');
				})->get();
			$view = View::make('search-listing-results', compact('searchResults'))->render();
            return Response::json(['html' => $view]);
		}
	}
}




